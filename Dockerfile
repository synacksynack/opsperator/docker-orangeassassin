FROM docker.io/python:3-slim-buster

# OrangeAssassin image for OpenShift Origin

ARG DO_UPGRADE=
ENV DEBIAN_FRONTEND=noninteractive

LABEL io.k8s.description="OrangeAssassin Image." \
      io.k8s.display-name="OrangeAssassin" \
      io.openshift.expose-services="1783:tcp" \
      io.openshift.tags="antispam,orange,orangeassassin" \
      io.openshift.non-scalable="false" \
      help="For more information visit https://gitlab.com/synacksynack/opsperator/docker-orange" \
      maintainer="Samuel MARTIN MORO <faust64@gmail.com>" \
      version="1.2b"

WORKDIR /usr/src/app

COPY config /usr/src/app

RUN set -x \
    && apt-get update \
    && mv run-orangeassassin.sh / \
    && if test "$DO_UPGRADE"; then \
	echo "# Upgrade Base Image"; \
	apt-get -y upgrade; \
	apt-get -y dist-upgrade; \
    fi \
    && echo "# Install OrangeAssassin" \
    && pip install -r requirements/python3.txt \
    && pip install -r requirements/base.txt \
    && if test "$USE_SQLALCHEMY"; then \
	pip install sqlalchemy; \
    else \
	pip install pymysql; \
    fi \
    && apt-get install -y wget \
    && python setup.py install \
    && mkdir -p /etc/orangesite /etc/orangeassassin \
    && mv local.cf /etc/orangeassassin/ \
    && echo "# Fixing Permissions" \
    && chown -R 1001:0 /etc/orangesite /etc/orangeassassin \
    && chmod -R g=u /etc/orangesite /etc/orangeassassin \
    && echo "# Cleaning Up" \
    && apt-get remove -y --purge wget \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/* /usr/share/doc /usr/share/man \
    && unset HTTP_PROXY HTTPS_PROXY NO_PROXY DO_UPGRADE http_proxy https_proxy

USER 1001
ENTRYPOINT [ "/run-orangeassassin.sh" ]
