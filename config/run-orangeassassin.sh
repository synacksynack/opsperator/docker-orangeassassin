#!/bin/sh

ORANGE_CONFIG=${ORANGE_CONFIG:-/etc/orangeassassin}
ORANGE_PORT=${ORANGE_PORT:-1783}
ORANGE_SITE=${ORANGE_SITE:-/etc/orangesite}

if test "$DEBUG" = sleep; then
    sleep 3600
elif test "$DEBUG"; then
    set -x
    exec python /usr/src/app/scripts/oad.py --prefork 4 --show-unknown \
	--log-file /dev/stdout --configpath ${ORANGE_CONFIG} --allow-tell \
	--sitepath ${ORANGE_SITE} --port $ORANGE_PORT -D
else
    exec python /usr/src/app/scripts/oad.py --prefork 4 --show-unknown \
	--log-file /dev/stdout --configpath ${ORANGE_CONFIG} --allow-tell \
	--sitepath ${ORANGE_SITE} --port $ORANGE_PORT
fi
