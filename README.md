# k8s OrangeAssassin

OrangeAssassin fo OpenShift

Forked from https://github.com/SpamExperts/OrangeAssassin

Build with:
```
$ make build
```

Run with:
```
$ make run
```

Start Demo in OpenShift:

```
$ make ocdemo
```

Cleanup OpenShift assets:

```
$ make ocpurge
```

|    Variable name      |    Description                | Default               |
| :-------------------- | ----------------------------- | --------------------- |
|  `ORANGE_CONFIG`      | OrangeAssassin Config Path    | `/etc/orangeassassin` |
|  `ORANGE_PORT`        | OrangeAssassin Bind Port      | `1783`                |
|  `ORANGE_SITE`        | OrangeAssassin Site Path      | `/etc/orangesite`     |


You can also set the following mount points by passing the `-v /host:/container`
flag to Docker.

|  Volume mount point         | Description                           |
| :-------------------------- | ------------------------------------- |
|  `/etc/orangeassassin`      | OrangeAssassin Rules Configuration    |
|  `/etc/orangesite`          | OrangeAssassin Site Configuration     |
